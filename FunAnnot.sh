#!/usr/bin/env bash


# FunAnnot - Fungal Secreted Proteins (or Secretome) Annotation Pipeline.
# Copyright (C) 2019 João Baptista <baptista.joao33@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


set -euo pipefail

# Variables initialization

SCRIPT_DIR="$(dirname "$0")"
export SCRIPT_DIR
export INPUT_DIR=""
export INPUT_FILE=""
export FILE_NAME=""
export OUTPUT=""
export EVALUE_PFAM="0.0000000001"	# default value
export EVALUE_LED="0.0000000001"	# default value
export EVALUE_CAZ="0.0000000001"	# default value
export EVALUE_MEROPS="0.0000000001"	# default value
export DOMEVALUE_PFAM="10" # default value
export DOMEVALUE_LED="10" # default value
export DOMEVALUE_CAZ="10" # default value
export DOMEVALUE_MEROPS="10" # default value
export F1_PFAM="0.02" # default value
export F1_LED="0.02" # default value
export F1_CAZ="0.02" # default value
export F1_MEROPS="0.02" # default value
export F2_PFAM="0.001" # default value
export F2_LED="0.001" # default value
export F2_CAZ="0.001" # default value
export F2_MEROPS="0.001" # default value
export F3_PFAM="0.00005" # default value
export F3_LED="0.00005" # default value
export F3_CAZ="0.00005" # default value
export F3_MEROPS="0.00005" # default value
FLAG_INPUT_DIR=0
FLAG_INPUT_FILE=0
FLAG_OUTPUT=0

# First parse the options h and v, even if they are the last options in the command line.

for i in "$@"; do
	case $i in
			-h)
				echo -e "Usage:

	./FunAnnot.sh -[OPTION] [ARGUMENT]

Example:

	./FunAnnot.sh -f input.fa -o output_dir 

General Options:

	-d DIR,		Input directory (for multiple files). The headers must not contain spaces.
	-f FILE,	Input file. The headers must not contain spaces.
	-o OUTPUT,	Output directory.

Pfam Annotation Options:

	-q N, First filter threshold for the MSV filter step. The default is \"0.02\".
	-w N, Second filter threshold for the Viterbi filter step. The default is \"0.001\".
	-e N, Third filter threshold for the Forward filter step. The default is \"1e-5\".
	-r N, Report target sequences with an E-value less than or equal to N. The default is \"1e-10\".
	-t N, Report individual domains with a conditional E-value less than or equal to N. The default is \"10.0\".

CAZymes Annotation Options:

	-y N, First filter threshold for the MSV filter step. The default is \"0.02\".
	-u N, Second filter threshold for the Viterbi filter step. The default is \"0.001\".
	-i N, Third filter threshold for the Forward filter step. The default is \"1e-5\".
	-p N, Report target sequences with an E-value less than or equal to N. The default is \"1e-10\".
	-a N, Report individual domains with a conditional E-value less than or equal to N. The default is \"10.0\".

Lipase Annotation Options:

	-s N, First filter threshold for the MSV filter step. The default is \"0.02\".	
	-g N, Second filter threshold for the Viterbi filter step. The default is \"0.001\".
	-j N, Third filter threshold for the Forward filter step. The default is \"1e-5\". 
	-k N, Report target sequences with an E-value less than or equal to N. The default is \"1e-10\".
	-l N, Report individual domains with a conditional E-value less than or equal to N. The default is \"10.0\".

Peptidase Annotation Options:

	-z N, First filter threshold for the MSV filter step. The default is \"0.02\".
	-x N, Second filter threshold for the Viterbi filter step. The default is \"0.001\".
	-c N, Third filter threshold for the Forward filter step. The default is \"1e-5\".
	-b N, Report target sequences with an E-value less than or equal to N. The default is \"1e-10\".
	-n N, Report individual domains with a conditional E-value less than or equal to N. The default is \"10.0\".

Miscellaneous:

	-h,		Displays this message.
	-v,		Displays version.

The options -d or -f and -o and their respective arguments are mandatory, the rest of the options are optional. For more information read the README.md file.

Please cite FunAnnot as well as all the programs that are used in the pipeline. Thank you!"
				exit 0
			;;
			-v)
				echo -e "Version: 1.0
Last Updated: 2019-10-08."
				exit 0 
			;;
	esac
done

# Parse the rest of the options

while getopts ":d:f:o:q:w:e:r:t:y:u:i:p:a:s:g:j:k:l:z:x:c:b:n:vh" OPT; do
	case "$OPT" in
		d)
			INPUT_DIR="$OPTARG"
			if [ -e "$INPUT_DIR" ];	then
				if [ -d "$INPUT_DIR" ];	then
					if [ ! "$(ls -A "$INPUT_DIR")" ]; then	# if the directory is empty or not
						echo -e "\n$INPUT_DIR is empty. Use -h for more information."
						exit 1
					else
						for f in "$INPUT_DIR"/*; do
							if [ -f "$f" ]; then
								if [ ! -s "$f" ]; then
									echo -e "\n$INPUT_DIR/$f is empty. Use -h for more information."
									exit 1
								else 
									if [ "$("$SCRIPT_DIR"/bin/FunAnnot/Parser.py "$f")" == "ERROR" ]; then
										echo -e "\n$f is not a FASTA file"
										exit 1
									elif [ "$("$SCRIPT_DIR"/bin/FunAnnot/Parser.py "$f")" == "ERROR1" ]; then
										echo -e "\n$f headers contain spaces"
										exit 1
									elif [ "$("$SCRIPT_DIR"/bin/FunAnnot/Parser.py "$f")" == "ERROR2" ]; then
										echo -e "\n$f contains illegal characters"
										exit 1
									else
										FLAG_INPUT_DIR=1
									fi
								fi 
							else
								echo -e "\n$INPUT_DIR/$f is not a file. Use -h for more information."
								exit 1
							fi
						done 
					fi
				else
					echo -e "\n$INPUT_DIR is not a directory. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\n$INPUT_DIR does not exist. Use -h for more information."
				exit 1
			fi
			;;
		f)
			INPUT_FILE="$OPTARG"
			if [ -e "$INPUT_FILE" ]; then
				if [ -f "$INPUT_FILE" ]; then
					if [ ! -s "$INPUT_FILE" ]; then
						echo -e "\n$INPUT_FILE is empty. Use -h for more information."
						exit 1
					else
						BASENAME="$(basename "$INPUT_FILE")"
						FILE_NAME="${BASENAME%.*}"
						unset BASENAME 
						if [ "$("$SCRIPT_DIR"/bin/FunAnnot/Parser.py "$INPUT_FILE")" == "ERROR" ]; then
							echo -e "\n$INPUT_FILE is not a FASTA file"
							exit 1
						elif [ "$("$SCRIPT_DIR"/bin/FunAnnot/Parser.py "$INPUT_FILE")" == "ERROR1" ]; then
							echo -e "\n$INPUT_FILE headers contain spaces"
							exit 1
						elif [ "$("$SCRIPT_DIR"/bin/FunAnnot/Parser.py "$INPUT_FILE")" == "ERROR2" ]; then
							echo -e "\n$INPUT_FILE contains illegal characters"
							exit 1
						else 
							FLAG_INPUT_FILE=1
						fi
					fi
				else
					echo -e "\n$INPUT_FILE is not a file. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\n$INPUT_FILE does not exist. Use -h for more information."
				exit 1
			fi
			;;
		o)
			OUTPUT="$OPTARG"
			if [ -e "$OUTPUT" ]; then
				if [ ! -d "$OUTPUT" ]; then
					echo -e "\n$OUTPUT is not a directory. Use -h for more information."
					exit 1
				else
					FLAG_OUTPUT=1
				fi
			else
				mkdir -p "$OUTPUT"/FunAnnot_Output
			fi
			;;
		q)
			F1_PFAM="$OPTARG"
			if [[ $F1_PFAM =~ ^-?[0-9]+$ ]] || [[ $F1_PFAM =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F1_PFAM" -le 0 ] || [ "$F1_PFAM" -ge 100 ]; then
					echo -e "\nThe F1 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F1 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		w)
			F2_PFAM="$OPTARG"
			if [[ $F2_PFAM =~ ^-?[0-9]+$ ]] || [[ $F2_PFAM =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F2_PFAM" -le 0 ] || [ "$F2_PFAM" -ge 100 ]; then
					echo -e "\nThe F2 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F2 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		e)
			F3_PFAM="$OPTARG"
			if [[ $F3_PFAM =~ ^-?[0-9]+$ ]] || [[ $F3_PFAM =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F3_PFAM" -le 0 ] || [ "$F3_PFAM" -ge 100 ]; then
					echo -e "\nThe F3 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F3 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		r)
			EVALUE_PFAM="$OPTARG"
			if [[ $EVALUE_PFAM =~ ^-?[0-9]+$ ]] || [[ $EVALUE_PFAM =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$EVALUE_PFAM" -le 0 ] || [ "$EVALUE_PFAM" -ge 100 ]; then
					echo -e "\nThe E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		t)
			DOMEVALUE_PFAM="$OPTARG"
			if [[ $DOMEVALUE_PFAM =~ ^-?[0-9]+$ ]] || [[ $DOMEVALUE_PFAM =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$DOMEVALUE_PFAM" -le 0 ] || [ "$DOMEVALUE_PFAM" -ge 100 ]; then
					echo -e "\nThe domains E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe domains E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		y)
			F1_CAZ="$OPTARG"
			if [[ $F1_CAZ =~ ^-?[0-9]+$ ]] || [[ $F1_CAZ =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F1_CAZ" -le 0 ] || [ "$F1_CAZ" -ge 100 ]; then
					echo -e "\nThe F1 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F1 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		u)
			F2_CAZ="$OPTARG"
			if [[ $F2_CAZ =~ ^-?[0-9]+$ ]] || [[ $F2_CAZ =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F2_CAZ" -le 0 ] || [ "$F2_CAZ" -ge 100 ]; then
					echo -e "\nThe F2 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F2 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		i)
			F3_CAZ="$OPTARG"
			if [[ $F3_CAZ =~ ^-?[0-9]+$ ]] || [[ $F3_CAZ =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F3_CAZ" -le 0 ] || [ "$F3_CAZ" -ge 100 ]; then
					echo -e "\nThe F3 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F3 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		p)
			EVALUE_CAZ="$OPTARG"
			if [[ $EVALUE_CAZ =~ ^-?[0-9]+$ ]] || [[ $EVALUE_CAZ =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$EVALUE_CAZ" -le 0 ] || [ "$EVALUE_CAZ" -ge 100 ]; then
					echo -e "\nThe E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		a)
			DOMEVALUE_CAZ="$OPTARG"
			if [[ $DOMEVALUE_CAZ =~ ^-?[0-9]+$ ]] || [[ $DOMEVALUE_CAZ =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$DOMEVALUE_CAZ" -le 0 ] || [ "$DOMEVALUE_CAZ" -ge 100 ]; then
					echo -e "\nThe domains E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe domains E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		s)
			F1_LED="$OPTARG"
			if [[ $F1_LED =~ ^-?[0-9]+$ ]] || [[ $F1_LED =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F1_LED" -le 0 ] || [ "$F1_LED" -ge 100 ]; then
					echo -e "\nThe F1 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F1 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		g)
			F2_LED="$OPTARG"
			if [[ $F2_LED =~ ^-?[0-9]+$ ]] || [[ $F2_LED =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F2_LED" -le 0 ] || [ "$F2_LED" -ge 100 ]; then
					echo -e "\nThe F2 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F2 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		j)
			F3_LED="$OPTARG"
			if [[ $F3_LED =~ ^-?[0-9]+$ ]] || [[ $F3_LED =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F3_LED" -le 0 ] || [ "$F3_LED" -ge 100 ]; then
					echo -e "\nThe F3 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F3 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		k)
			EVALUE_LED="$OPTARG"
			if [[ $EVALUE_LED =~ ^-?[0-9]+$ ]] || [[ $EVALUE_LED =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$EVALUE_LED" -le 0 ] || [ "$EVALUE_LED" -ge 100 ]; then
					echo -e "\nThe E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		l)
			DOMEVALUE_LED="$OPTARG"
			if [[ $DOMEVALUE_LED =~ ^-?[0-9]+$ ]] || [[ $DOMEVALUE_LED =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$DOMEVALUE_LED" -le 0 ] || [ "$DOMEVALUE_LED" -ge 100 ]; then
					echo -e "\nThe domains E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe domains E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		z)
			F1_MEROPS="$OPTARG"
			if [[ $F1_MEROPS =~ ^-?[0-9]+$ ]] || [[ $F1_MEROPS =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F1_MEROPS" -le 0 ] || [ "$F1_MEROPS" -ge 100 ]; then
					echo -e "\nThe F1 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F1 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		x)
			F2_MEROPS="$OPTARG"
			if [[ $F2_MEROPS =~ ^-?[0-9]+$ ]] || [[ $F2_MEROPS =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F2_MEROPS" -le 0 ] || [ "$F2_MEROPS" -ge 100 ]; then
					echo -e "\nThe F2 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F2 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		c)
			F3_MEROPS="$OPTARG"
			if [[ $F3_MEROPS =~ ^-?[0-9]+$ ]] || [[ $F3_MEROPS =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$F3_MEROPS" -le 0 ] || [ "$F3_MEROPS" -ge 100 ]; then
					echo -e "\nThe F3 value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe F3 value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		b)
			EVALUE_MEROPS="$OPTARG"
			if [[ $EVALUE_MEROPS =~ ^-?[0-9]+$ ]] || [[ $EVALUE_MEROPS =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$EVALUE_MEROPS" -le 0 ] || [ "$EVALUE_MEROPS" -ge 100 ]; then
					echo -e "\nThe E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		n)
			DOMEVALUE_MEROPS="$OPTARG"
			if [[ $DOMEVALUE_MEROPS =~ ^-?[0-9]+$ ]] || [[ $DOMEVALUE_MEROPS =~ ^-?[0-9]+\.[0-9]+$ ]]; then
				if [ "$DOMEVALUE_MEROPS" -le 0 ] || [ "$DOMEVALUE_MEROPS" -ge 100 ]; then
					echo -e "\nThe domains E-value must be greater than 0 and less than 100. Use -h for more information."
					exit 1
				fi
			else
				echo -e "\nThe domains E-value must be an integer or a floating point. Use -h for more information."
				exit 1
			fi
			;;
		v)
			continue
			;;
		h)
			continue
			;;
		\?)
			echo -e "\nInvalid option: -$OPTARG. Use -h for more information."
			exit 1
			;;
		:)
			echo -e "\nThe option -$OPTARG needs an argument. Use -h for more information."
			exit 1
			;;
	esac
done

# Exits if both -f and -d were given

if [ "$FLAG_INPUT_DIR" -eq 1 ] && [ "$FLAG_INPUT_FILE" -eq 1 ]; then
	echo -e "\nSelect either -f or -d. Use -h for more information."
	exit 1
fi

# if $OUTPUT/FunAnnot_Output exists it will ask for permission to overwrite.

overwrite() {
	if [ -e "$OUTPUT"/FunAnnot_Output ]; then 
		echo -ne "\n$OUTPUT/FunAnnot_Output already exists, this will overwrite it. Do you want to continue? [Y/n] "
		read -r FLAG_OVERWRITE
		if [ "$FLAG_OVERWRITE" == "yes" ] || [ "$FLAG_OVERWRITE" == "y" ] || [ "$FLAG_OVERWRITE" == "Yes" ] || [ "$FLAG_OVERWRITE" == "Y" ]; then
			echo -e "\nOverwriting $OUTPUT/FunAnnot_Output ..."
			rm -rf "$OUTPUT"/FunAnnot_Output && \
			mkdir "$OUTPUT"/FunAnnot_Output
		else
			echo -e "\nExiting..."
			exit 0
		fi
	else
		mkdir "$OUTPUT"/FunAnnot_Output
	fi
}

# Runs the functions to check the -d or -f, -o and -p options and runs the corresponding script. 

if [ "$FLAG_INPUT_DIR" -eq 1 ] && [ "$FLAG_OUTPUT" -eq 1 ]; then # if the -d and -o options were given
	overwrite && \
	echo -e "\nThe program $0 is running, it may take a while."
	bash "$SCRIPT_DIR"/bin/FunAnnot/Dir.sh | \
	tee -a "$OUTPUT"/FunAnnot_Output/FunAnnot.log 
	exit 0
elif [ "$FLAG_INPUT_FILE" -eq 1 ] && [ "$FLAG_OUTPUT" -eq 1 ]; then # if the -f and -o options were given
	overwrite && \
	echo -e "\nThe program $0 is running, it may take a while."
	bash "$SCRIPT_DIR"/bin/FunAnnot/File.sh | \
	tee -a "$OUTPUT"/FunAnnot_Output/FunAnnot.log
	exit 0
else
	echo -e "\nThe options -d or -f and -o and their respective arguments must be specified. Use -h for more information."
	exit 1
fi

