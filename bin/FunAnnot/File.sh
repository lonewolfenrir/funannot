#!/usr/bin/env bash


# FunAnnot - Fungal Secreted Proteins (or Secretome) Annotation Pipeline.
# Copyright (C) 2019 João Baptista <baptista.joao33@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


set -euo pipefail

# Trap

trap 'find "$OUTPUT"/FunAnnot_Output -empty -delete' SIGHUP SIGINT SIGTERM SIGQUIT ERR EXIT

# Citation 

citation() {
	echo -e "\nPlease cite FunAnnot as well as all the programs that are used in the pipeline. Thank you!"
}

# Pfam

echo -e "\nRunning the Pfam annotation...\n"
mkdir "$OUTPUT"/FunAnnot_Output/Pfam
hmmscan -E "$EVALUE_PFAM" --domE "$DOMEVALUE_PFAM" --F1 "$F1_PFAM" --F2 "$F2_PFAM" --F3 "$F3_PFAM" --tblout "$OUTPUT"/FunAnnot_Output/Pfam/"$FILE_NAME".log "$SCRIPT_DIR"/data/Pfam/Pfam-A.hmm "$INPUT_FILE" > /dev/null
sed "/^#/d" "$OUTPUT"/FunAnnot_Output/Pfam/"$FILE_NAME".log | \
awk '{print $3","$2","$5","$6","$7","$8","$9","$10","$11","$12","$13","$14","$15","$16","$17","$18}' | \
tee "$OUTPUT"/FunAnnot_Output/Pfam/"$FILE_NAME".csv | \
awk 'BEGIN{FS=","}{print $1" - "$2}'
if [ ! -s "$OUTPUT"/FunAnnot_Output/Pfam/"$FILE_NAME".csv ]; then
	echo -e "No proteins were annotated"
	find "$OUTPUT"/FunAnnot_Output/Pfam -type f -name "$FILE_NAME".csv -delete
fi
echo -e "\nFinished. (Runtime - $SECONDS seconds)"

# dbCAN2

echo -e "\nRunning the CAZymes annotation...\n"
mkdir "$OUTPUT"/FunAnnot_Output/dbCAN2 
hmmscan -E "$EVALUE_CAZ" --domE "$DOMEVALUE_CAZ" --F1 "$F1_CAZ" --F2 "$F2_CAZ" --F3 "$F3_CAZ" --tblout "$OUTPUT"/FunAnnot_Output/dbCAN2/"$FILE_NAME".log "$SCRIPT_DIR"/data/dbCAN2/dbCAN-HMMdb-V7.txt "$INPUT_FILE" > /dev/null
sed "/^#/d" "$OUTPUT"/FunAnnot_Output/dbCAN2/"$FILE_NAME".log | \
awk '{print $3","$1","$5","$6","$7","$8","$9","$10","$11","$12","$13","$14","$15","$16","$17","$18}' | \
sed 's/.hmm//' | \
tee "$OUTPUT"/FunAnnot_Output/dbCAN2/"$FILE_NAME".csv | \
awk 'BEGIN{FS=","}{print $1" - "$2}'
if [ ! -s "$OUTPUT"/FunAnnot_Output/dbCAN2/"$FILE_NAME".csv ]; then
	echo -e "No proteins were annotated"
	find "$OUTPUT"/FunAnnot_Output/dbCAN2 -type f -name "$FILE_NAME".csv -delete 
fi
echo -e "\nFinished. (Runtime - $SECONDS seconds)"

# LED

echo -e "\nRunning the lipase annotation...\n"
mkdir "$OUTPUT"/FunAnnot_Output/LED
hmmscan -E "$EVALUE_LED" --domE "$DOMEVALUE_LED" --F1 "$F1_LED" --F2 "$F2_LED" --F3 "$F3_LED" --tblout "$OUTPUT"/FunAnnot_Output/LED/"$FILE_NAME".log "$SCRIPT_DIR"/data/LED/all.hmm "$INPUT_FILE" > /dev/null
sed "/^#/d" "$OUTPUT"/FunAnnot_Output/LED/"$FILE_NAME".log | \
awk '{print $3","$1","$5","$6","$7","$8","$9","$10","$11","$12","$13","$14","$15","$16","$17","$18}' | \
tee "$OUTPUT"/FunAnnot_Output/LED/"$FILE_NAME".csv | \
awk 'BEGIN{FS=","}{print $1" - "$2}'
if [ ! -s "$OUTPUT"/FunAnnot_Output/LED/"$FILE_NAME".csv ]; then
	echo -e "No proteins were annotated"
	find "$OUTPUT"/FunAnnot_Output/LED -type f -name "$FILE_NAME".csv -delete 
fi
echo -e "\nFinished. (Runtime - $SECONDS seconds)"

# MEROPS

echo -e "\nRunning the peptidase annotation...\n"
mkdir "$OUTPUT"/FunAnnot_Output/MEROPS
phmmer -E "$EVALUE_MEROPS" --domE "$DOMEVALUE_MEROPS" --F1 "$F1_MEROPS" --F2 "$F2_MEROPS" --F3 "$F3_MEROPS" --tblout "$OUTPUT"/FunAnnot_Output/MEROPS/"$FILE_NAME".log "$INPUT_FILE" "$SCRIPT_DIR"/data/MEROPS/merops_scan.lib > /dev/null
sed "/^#/d" "$OUTPUT"/FunAnnot_Output/MEROPS/"$FILE_NAME".log | \
awk '{print $3","$1","$5","$6","$7","$8","$9","$10","$11","$12","$13","$14","$15","$16","$17","$18}' | \
tee "$OUTPUT"/FunAnnot_Output/MEROPS/"$FILE_NAME".csv | \
awk 'BEGIN{FS=","}{print $1" - "$2}'
if [ ! -s "$OUTPUT"/FunAnnot_Output/MEROPS/"$FILE_NAME".csv ]; then
	echo -e "No proteins were annotated"
	find "$OUTPUT"/FunAnnot_Output/MEROPS -type f -name "$FILE_NAME".csv -delete 
fi
echo -e "\nFinished. (Runtime - $SECONDS seconds)"

# EffectorP 2.0

echo -e "\nRunning the effectors annotation...\n"
mkdir "$OUTPUT"/FunAnnot_Output/EffectorP 
python2 "$SCRIPT_DIR"/bin/EffectorP_2.0/Scripts/EffectorP.py -i "$INPUT_FILE" 2> /dev/null | \
tee "$OUTPUT"/FunAnnot_Output/EffectorP/"$FILE_NAME".log | \
awk '/Effector probability/' | \
awk '{print $1","$3}' |	\
sed 's/probability://' | \
tee "$OUTPUT"/FunAnnot_Output/EffectorP/"$FILE_NAME".csv | \
awk 'BEGIN{FS=","}{print $1" - ""Effector"}'
if [ ! -s "$OUTPUT"/FunAnnot_Output/EffectorP/"$FILE_NAME".csv ]; then
	echo -e "No proteins were annotated"
	find "$OUTPUT"/FunAnnot_Output/EffectorP -type f -name "$FILE_NAME".csv -delete 
fi
echo -e "\nFinished. (Runtime - $SECONDS seconds)"

# Final Message

echo -e "\n$0 has finished (Runtime - $SECONDS seconds). The annotations can be found in $OUTPUT/FunAnnot_Output."
citation
exit 0

