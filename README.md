# FunSec - Fungal Secreted Proteins (or Secretome) Prediction Pipeline #

## Description ##

FunAnnot was developed with the purpose of automatically annotate the predicted fungal secreted proteins provided by the FunSec pipeline. It allows for the annotation of Pfam families, CArbohydrate-active enZymes (CAZymes), lipases, peptidases and effectors.

The annotation is made by querying the Pfam, LED and dbCAN2 HMM databases, and the MEROPS database with the hmmscan and phmmer programs, respectively. Both programs are part of the HMMER software package. To annotate the effectors, the EffectorP 2.0 program is used.

## Requirements ##

This pipeline only runs in **GNU/Linux**. It's necessary to install the programs used in the pipeline, so users will have to download and setup each program individually and move it into the FunAnnot's bin directory.

### Installation ###

Please follow the instructions below for how to setup up each program.

##### HMMER software package #####

To download the HMMER software package, please check if it's present in your GNU/Linux distribution repositories and install it accordingly.

##### EffectorP 2.0 #####

To download EffectorP 2.0, visit this <http://effectorp.csiro.au/EffectorP_2.0.tar.gz> page. Users should receive a tar.gz file. To untar the file use the following command in your terminal:

```
tar -zxvf FILE.tar.gz
```

This will create a directory named EffectorP_2.0. The directory must be moved into the FunAnnot's bin directory. Then proceed to run the following commands:

```
chmod -R 755 EffectorP_2.0/
cd EffectorP_2.0
cd Scripts
tar xvf emboss-latest.tar.gz
cd EMBOSS-6.5.7/
./configure
make
cd ../
unzip weka-3-8-1.zip
```

## Usage ##

The pipeline only works with **FASTA** files and due to the software's limitations, the headers must not contain any spaces. To run it please use the following code in the GNU/Linux command-line:

```
./FunAnnot.sh -[OPTION] [ARGUMENT]
```

## Options ##

General Options:

```
	-d DIR,		Input directory (for multiple FASTA files). The headers must not contain spaces.
	-f FILE,	Input FASTA file. The headers must not contain spaces.
	-o OUTPUT,	Output directory.
```

Pfam Annotation Options:

```
	-q N,	First filter threshold for the MSV filter step. The default is "0.02".
	-w N,	Second filter threshold for the Viterbi filter step. The default is "0.001".
	-e N,	Third filter threshold for the Forward filter step. The default is "1e-5".
	-r N,	Report target sequences with an E-value less than or equal to N. The default is "1e-10".
	-t N,	Report individual domains with a conditional E-value less than or equal to N. The default is "10.0".
```

CAZymes Annotation Options:

```
	-y N,	First filter threshold for the MSV filter step. The default is "0.02".
	-u N,	Second filter threshold for the Viterbi filter step. The default is "0.001".
	-i N,	Third filter threshold for the Forward filter step. The default is "1e-5".
	-p N,	Report target sequences with an E-value less than or equal to N. The default is "1e-10".
	-a N,	Report individual domains with a conditional E-value less than or equal to N. The default is "10.0".
```

Lipase Annotation Options:

```
	-s N,	First filter threshold for the MSV filter step. The default is "0.02".
	-g N,	Second filter threshold for the Viterbi filter step. The default is "0.001".
	-j N,	Third filter threshold for the Forward filter step. The default is "1e-5".
	-k N,	Report target sequences with an E-value less than or equal to N. The default is "1e-10".
	-l N,	Report individual domains with a conditional E-value less than or equal to N. The default is "10.0".
```

Peptidase Annotation Options:

```
	-z N,	First filter threshold for the MSV filter step. The default is "0.02".
	-x N,	Second filter threshold for the Viterbi filter step. The default is "0.001".
	-c N,	Third filter threshold for the Forward filter step. The default is "1e-5".
	-b N,	Report target sequences with an E-value less than or equal to N. The default is "1e-10".
	-n N,	Report individual domains with a conditional E-value less than or equal to N. The default is "10.0".
```

Miscellaneous:

```
	-h,		Displays this message.
	-v,		Displays version.
```

The options -d or -f and -o and their respective arguments are mandatory, the rest of the options are optional. For more information read the README.md file.

## Citation ##

Please cite FunAnnot as well as all the programs that are used in the pipeline. Thank you!

## Contact ##

For further information or feedback please open an [issue](https://gitlab.com/lonewolfenrir/funannot/issues).

## License ##

GPLv3, see LICENSE file for more information.
